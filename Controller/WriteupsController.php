<?php
App::uses('AppController', 'Controller');
/**
 * Writeups Controller
 *
 * @property Writeup $Writeup
 */
class WriteupsController extends AppController {

    public $uses = array('Team', 'Writeup');
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('index');
    }
/**
 * index method
 *
 * @return void
 */
	public function index() {
        $writeups = $this->Writeup->find('all',
            array(
                "order" => array(
                    "Team.score" => "desc",
                    "Team.solutiontime" =>"ASC",),
            ));
		$this->set('writeups', $writeups);
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        $this->redirect(array('action' => 'index'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
		if ($this->request->is('post')) {
			$this->Writeup->create();
			if ($this->Writeup->save($this->request->data)) {
				$this->Session->setFlash(__('The writeup has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The writeup could not be saved. Please, try again.'));
			}
		}
		$teams = $this->Writeup->Team->find('list');
		$this->set(compact('teams'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
		$this->Writeup->id = $id;
		if (!$this->Writeup->exists()) {
			throw new NotFoundException(__('Invalid writeup'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Writeup->save($this->request->data)) {
				$this->Session->setFlash(__('The writeup has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The writeup could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Writeup->read(null, $id);
		}
		$teams = $this->Writeup->Team->find('list');
		$this->set(compact('teams'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Writeup->id = $id;
		if (!$this->Writeup->exists()) {
			throw new NotFoundException(__('Invalid writeup'));
		}
		if ($this->Writeup->delete()) {
			$this->Session->setFlash(__('Writeup deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Writeup was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
