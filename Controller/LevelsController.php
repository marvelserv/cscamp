<?php

App::uses('AppController', 'Controller');

/**
 * Levels Controller
 *
 * @property Level $Level
 */
class LevelsController extends AppController {

    public $uses = array('Level', 'TeamsLevel', 'Team');

    public function solveLevel($id = null) {
        $x = array();
        $mslev = $this->TeamsLevel->find('all',array(
            'fields' => array('level_id'),
            'conditions' => array('team_id' => $this->Session->read('Auth.User.team_id'))
        ));

        $solvedLevels = $this->TeamsLevel->find('list',array(
            'fields' => array('level_id'),
            'conditions' => array('team_id' => $this->Session->read('Auth.User.team_id'))
        ));
        foreach ($mslev as $y) {
            array_push($x, $y['TeamsLevel']['level_id']);
        }
        $this->Level->id = $this->request->data['Level']['level_id'];
        $level = $this->Level->read();
        $providedKey = $this->request->data['Level']['providedKey'];
        $this->Team->id = $this->Auth->user("team_id");
        $team = $this->Team->read();
        //Number of teams that solved the level
        $solved = $this->Team->TeamsLevel->find('all', array(
            'conditions' => array(
                'level_id' => $this->Level->id,
                )));
        #echo "<pre>"; print_r($level);echo "</pre>"; exit();
        if (!in_array($this->Level->id, $x)) {
            if (!empty($providedKey) && $providedKey == $level['Level']['key']) {
                $this->Session->setFlash("Congrats, you have passed this level", 'flash_success');
                if (empty($team)) {
                    $team['Team']['score'] = $level['Level']['point'];
                } else {
                    $team['Team']['score'] += $level['Level']['point'];
                }
                $team['Team']['solutiontime'] = time();
                $this->Session->write('Auth.User.Team.score', $team['Team']['score']);
                $this->Team->id = $this->Session->read('Auth.User.Team.id');
                $this->Team->save($team);
                $solved['TeamsLevel']['team_id'] = $this->Team->id;
                $solved['TeamsLevel']['level_id'] = $this->Level->id;
                $this->TeamsLevel->create();
                $this->TeamsLevel->save($solved);
                $this->redirect('/');
            } else {
                $this->Session->setFlash("Wrong Key, try again", 'flash_error');
                $this->redirect('/');
            }
        } else {
            $this->Session->setFlash("You already solved this level", "flash_info");
            $this->redirect('/');
        }
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        $this->Level->recursive = 0;
        $this->set('levels', $this->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        $this->Level->id = $id;
        if (!$this->Level->exists()) {
            throw new NotFoundException(__('Invalid level'));
        }
        $this->set('level', $this->Level->read(null, $id));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        if ($this->request->is('post')) {
            $this->Level->create();
            if ($this->Level->save($this->request->data)) {
                $this->Session->setFlash(__('The level has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The level could not be saved. Please, try again.'));
            }
        }
        $categories = $this->Level->Category->find('list');
        $this->set(compact('categories'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        $this->Level->id = $id;
        if (!$this->Level->exists()) {
            throw new NotFoundException(__('Invalid level'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Level->save($this->request->data)) {
                $this->Session->setFlash(__('The level has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The level could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->Level->read(null, $id);
        }
        $categories = $this->Level->Category->find('list');
        $this->set(compact('categories'));
    }

    /**
     * delete method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Level->id = $id;
        if (!$this->Level->exists()) {
            throw new NotFoundException(__('Invalid level'));
        }
        if ($this->Level->delete()) {
            $this->Session->setFlash(__('Level deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Level was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

}
