<?php

App::uses('AppController', 'Controller');

/**
 * TeamsLevels Controller
 *
 * @property TeamsLevel $TeamsLevel
 */
class TeamsLevelsController extends AppController {

    public $paginate = array(
        'limit' => 25,
        "order" => array("team_id" => "asc"),
    );
    /**
     * index method
     *
     * @return void
     */
    public function index() {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        $data = $this->paginate('TeamsLevel');
        $this->set('teamsLevels', $data);
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        $this->TeamsLevel->id = $id;
        if (!$this->TeamsLevel->exists()) {
            throw new NotFoundException(__('Invalid teams level'));
        }
        $this->set('teamsLevel', $this->TeamsLevel->read(null, $id));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        if ($this->request->is('post')) {
            print_r($this->request->data);
            exit();
            $this->TeamsLevel->create();
            if ($this->TeamsLevel->save($this->request->data)) {
                $this->Session->setFlash(__('The teams level has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The teams level could not be saved. Please, try again.'));
            }
        }
        $teams = $this->TeamsLevel->Team->find('list');
        $levels = $this->TeamsLevel->Level->find('list');
        $this->set(compact('teams', 'levels'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        $this->TeamsLevel->id = $id;
        if (!$this->TeamsLevel->exists()) {
            throw new NotFoundException(__('Invalid teams level'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->TeamsLevel->save($this->request->data)) {
                $this->Session->setFlash(__('The teams level has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The teams level could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->TeamsLevel->read(null, $id);
        }
        $teams = $this->TeamsLevel->Team->find('list');
        $levels = $this->TeamsLevel->Level->find('list');
        $this->set(compact('teams', 'levels'));
    }

    /**
     * delete method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->TeamsLevel->id = $id;
        if (!$this->TeamsLevel->exists()) {
            throw new NotFoundException(__('Invalid teams level'));
        }
        if ($this->TeamsLevel->delete()) {
            $this->Session->setFlash(__('Teams level deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Teams level was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

}
