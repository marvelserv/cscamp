<?php

App::uses('AppController', 'Controller');

/**
 * Teams Controller
 *
 * @property Team $Team
 */
class TeamsController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('index', 'teamsList');
    }

    /**
     * index method
     *
     * @return void
     */
    public $uses = array('Team', 'User');

    public function teamsList() {
        $this->Team->recursive = 0;
        $teamList = $this->Team->find('all', array('fields' => array('Team.id', 'Team.name','Team.country', 'Team.created')));
        $this->set('teamsList', $teamList);
    }

    public function index() {
        $teams = $this->Team->find('all',
            array(
                "order" => array(
                    "Team.score" => "desc",
                    "Team.solutiontime" =>"asc",
                )));
        $this->set('teams', $teams);
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        $this->Team->id = $id;
        if (!$this->Team->exists()) {
            throw new NotFoundException(__('Invalid team'));
        }
        $this->set('team', $this->Team->read(null, $id));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            if (!$this->Auth->User('team_id') or parent::isAdmin($this->Auth->user())) {
                $this->Team->create();
                $userInfo = $this->User->findById($this->Session->read('Auth.User.id'));
                $this->User->id = $userInfo['User']['id'];
                if ($this->Team->save($this->request->data)) {
                    $team = $this->Team->find('first', array(
                        'conditions' => array(
                            'Team.name' => $this->request->data['Team']['name'],
                        )
                    ));
                    if (!parent::isAdmin($this->Auth->user())) {
                        $me['User']['team_id'] = $team['Team']['id'];
                    }
                    $this->Session->write('Auth.User.team_id', $team['Team']['id']);
                    $this->User->save($me);
                    $this->Session->setFlash('Successfully Created/Joined ' . $this->request->data['Team']['name'], 'flash_info');
                    $this->redirect('/');
                } else {
                    $this->Session->setFlash(__('The team could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash('You\'re already in a Team', 'flash_info');
                $this->redirect($this->referer());
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        $this->Team->id = $id;
        if (!$this->Team->exists()) {
            throw new NotFoundException(__('Invalid team'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Team->save($this->request->data)) {
                $this->Session->setFlash(__('The team has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The team could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->Team->read(null, $id);
        }
    }

    /**
     * delete method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Team->id = $id;
        if (!$this->Team->exists()) {
            throw new NotFoundException(__('Invalid team'));
        }
        if ($this->Team->delete()) {
            $this->Session->setFlash(__('Team deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Team was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

}
