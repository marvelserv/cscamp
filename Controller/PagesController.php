<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('display');

//            $this->redirect(array('controller' => 'pages', 'action' => 'display', 'home'));
    }

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('User', 'Team', 'Level', 'Category', 'TeamsLevel', 'News');

    /**
     * Displays a view
     *
     * @param string What page to display
     */
    public function display() {
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            $this->redirect('/');
        }
        $page = $subpage = $title = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        if (!empty($path[$count - 1])) {
            $title = Inflector::humanize($path[$count - 1]);
        }

        $teamName = $this->Team->findById($this->Session->read('Auth.User.team_id'));
        //$lev contains the solved levels
        $x = array();
        $mslev = $this->TeamsLevel->find('all', array('fields' => array('level_id'), 'conditions'=>array('team_id'=>$this->Session->read('Auth.User.team_id'))));
        foreach($mslev as $y){
            array_push($x, $y['TeamsLevel']['level_id']);
        }
        $lev = $this->TeamsLevel->find('list', array('fields' => array('level_id'), 'conditions'=>array('team_id'=>$this->Session->read('Auth.User.team_id'))));
        $teams = $this->User->Team->find('list');
        $topTeams = $this->Team->find('all', array(
            'order' => array(
                'score' => 'desc',
                "solutiontime" =>"asc",
            ),
            'limit' => 5,
        ));
        $solvedTimes = $this->TeamsLevel->find('list', array('fields'=>array('level_id')));
        $categories = $this->Category->find('all');
        $news = $this->News->find('all');
        //print_r($news); exit();
        //echo "<pre>";
        //print_r($this->Session->read('Auth.User.team_id'));
        //print_r($mslev);
        //print_r($this->Session->read('Auth.User.Team.id'));
        //print_r(array_values($lev));
        //echo "</pre>";
        //exit();
        $this->set(compact('teams', 'topTeams', 'categories', 'lev', 'teams','x', 'news', 'solvedTimes'));
        $this->set('teamName', $teamName['Team']['name']);
        $this->set(compact('page', 'subpage'));
        $this->set('title_for_layout', $title);
        $this->render(implode('/', $path));
    }

}
