<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {

    public $uses = array('User', 'Team');
    var $helpers = array('Time');
    public $paginate = array(
        'limit' => 10
    );

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('register', 'forgotPassword', 'resetPassword');
    }

    public function isThatYou($user) {
        return $this->Auth->user('id') === $user || $this->Auth->user('role') === 'admin';
    }

    public function register() {
        if ($this->Auth->user()) {
            $this->redirect('/');
        }
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->request->data['User']['password'] != $this->request->data['User']['rePassword']) {
                $this->Session->setFlash('Passwords don\'t match', 'flash_error');
                $this->redirect(array('controller' => 'pages', 'action' => 'display'));
            }

            $this->request->data['User']['role'] = "ctfer";
            $this->request->data['User']['hashed'] = AuthComponent::password($this->request->data['User']['email']);
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved'), 'flash_success');
                $this->redirect(array('action' => 'login'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'flash_error');
                $this->redirect(array('controller' => 'pages', 'action' => 'display'));
            }
        }
    }

    public function login() {
        if ($this->Auth->user()) {
            $this->redirect('/');
        }
        if ($this->request->is('post')) {
            App::Import('Utility', 'Validation');
            if (isset($this->data['User']['username']) && Validation::email($this->data['User']['username'])) {
                $this->request->data['User']['email'] = $this->data['User']['username'];
                $this->Auth->authenticate['Form'] = array('fields' => array('username' => 'email'));
//                print $this->data['User']['password'];
//                exit();
            }
            if ($this->Auth->login()) {

               if ($this->Auth->user('role') === 'admin'){ 
                   $this->redirect(array('controller'=>'teams'));}
                $this->redirect('/');
            } else {
                $this->Session->setFlash('Invalid username or password, try again', 'flash_error');
            }
        }
    }

    public function logout() {
        $this->redirect($this->Auth->logout());
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {
                if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
                if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->set('user', $this->User->read(null, $id));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->request->data['User']['password'] != $this->request->data['User']['rePassword']) {
                $this->Session->setFlash('Passwords don\'t match', 'flash_error');
                $this->redirect(array('controller' => 'Users', 'action' => 'add'));
            }
            $this->request->data['User']['hashed'] = AuthComponent::password($this->request->data['User']['email']);
            $this->request->data['User']['role'] = 'ctfer';
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        }
        $teams = $this->User->Team->find('list');
        $this->set(compact('teams'));
    }

    public function joinTeam() {
        $userInputKey = $this->request->data['User']['key'];
        $team = $this->Team->findById($this->request->data['User']['team_id']);
        $teamSecretKey = $team['Team']['secretkey'];
        if (!empty($userInputKey) && $userInputKey == $teamSecretKey) {
            $userInfo = $this->User->findById($this->Session->read('Auth.User.id'));
            $this->User->id = $userInfo['User']['id'];
            if (!$this->User->exists()) {
                throw new NotFoundException(__('Invalid user'));
            }
            if ($this->request->is('post') || $this->request->is('put')) {
                $me['User']['team_id'] = $this->request->data['User']['team_id'];
                if ($this->User->save($me)) {
                    $this->Session->setFlash('Congrats, you have joined ' . $team['Team']['name'], 'flash_success');
                    // Write on the session to redirect the user after joining a team
                    $this->Session->write('Auth.User.team_id', $this->request->data['User']['team_id']);
                    $this->redirect(array("controller" => "pages", "action" => "display", "home"));
                } else {
                    $this->Session->setFlash('Could not join the Team', 'flash_error');
                    $this->redirect($this->referer());
                }
            }
        } else {
            $this->Session->setFlash('Wrong Key', 'flash_error');
            $this->redirect($this->referer());
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        
        $loggedUserType = $this->Auth->user('role');
        $this->set('loggedUserType', $loggedUserType);
        $userId = $this->request->params['pass'][0];
        if (!$this->isThatYou($userId)) {
            $this->redirect('/');
        }
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['User']['role'] = 'ctfer';
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->User->read(null, $id);
        }
        $teams = $this->User->Team->find('list');
        $this->set(compact('teams'));
    }

    /**
     * delete method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
                if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->delete()) {
            $this->Session->setFlash(__('User deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('User was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

    public function forgotPassword() {
        if ($this->Auth->user()) {
            $this->redirect('/');
        }
        if ($this->request->is('post')) {
            $user = $this->User->find('first', array(
                'conditions' => array('User.email' => $this->request->data['User']['email'])
                    )
            );
            $this->User->id = $user['User']['id'];
            if (!$this->User->exists()) {
                throw new NotFoundException(__('Invalid user'));
            }
            $hashed = $user['User']['hashed'];
            $email = $user['User']['email'];
            $route = Router::url('/', true);
            $url = $route . "users/resetPassword?hash=" . $hashed;
            $message = <<<EOF
<html>
  <body>
    <center>
        <b>Click the link below to reset your password</b> <br><br>
        <a href="{$url}">{$url}</a><br><br><br>
        <font color="red">Thanks {$user['User']['username']}</font> <br>
    </center>
  </body>
</html>
EOF;

            $headers = "From: admin@{$route}\r\n";
            $headers .= "Content-type: text/html\r\n";
            mail($email, 'password reset', $message, $headers);
            $this->Session->setFlash('Check your email for activation link', 'flash_info');
        }
    }

    public function resetPassword() {
        if ($this->Auth->user()) {
            $this->redirect('/');
        }
        if ($this->request->is('post')) {
            $hash = $this->params->query['hash'];
            if ($this->request->data['User']['password'] != $this->request->data['User']['repassword']) {
                $this->Session->setFlash('Passwords don\'t match', 'flash_error');
                $this->redirect("/users/resetPassword?hash=" . $hash);
            } else {
                $user = $this->User->find('first', array(
                    'conditions' => array('User.hashed' => $hash)
                        ));
                $user['User']['hashed'] = AuthComponent::password($user['User']['hashed']);
                $newHash = $this->request->data['User']['password'];
                $this->User->id = $user['User']['id'];
//                echo $user['User']['id'];
//                exit();
                $user['User']['password'] = $newHash;
                if ($this->User->save($user)) {
                    $this->Session->setFlash('Changed password successfully', 'flash_success');
                    $this->redirect('/');
                }
            }
        }
    }

}
