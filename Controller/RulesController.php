<?php

App::uses('AppController', 'Controller');

/**
 * Rules Controller
 *
 * @property Rule $Rule
 */
class RulesController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('index');
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {

        $this->Rule->recursive = 0;
        $this->set('rules', $this->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        $this->Rule->id = $id;
        if (!$this->Rule->exists()) {
            throw new NotFoundException(__('Invalid rule'));
        }
        $this->set('rule', $this->Rule->read(null, $id));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        if ($this->request->is('post')) {
            $this->Rule->create();
            if ($this->Rule->save($this->request->data)) {
                $this->Session->setFlash(__('The rule has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The rule could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        $this->Rule->id = $id;
        if (!$this->Rule->exists()) {
            throw new NotFoundException(__('Invalid rule'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Rule->save($this->request->data)) {
                $this->Session->setFlash(__('The rule has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The rule could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->Rule->read(null, $id);
        }
    }

    /**
     * delete method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        if (!parent::isAdmin($this->Auth->user())) {
            $this->redirect('/');
        }
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Rule->id = $id;
        if (!$this->Rule->exists()) {
            throw new NotFoundException(__('Invalid rule'));
        }
        if ($this->Rule->delete()) {
            $this->Session->setFlash(__('Rule deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Rule was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

}
