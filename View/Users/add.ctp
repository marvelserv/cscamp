<div class="row-fluid">
    <div class="span2 offset1">
        <h3><?php echo __('Actions'); ?></h3>
        <ul class="nav nav-tabs nav-stacked">
            <li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
        </ul>
    </div>
    <div class="span8">
        <legend><?php echo __('Add User'); ?></legend>
        <?php echo $this->Form->create('User', array('class' => 'form-horizontal')); ?>
        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->input('name', array('label' => __('Name'))); ?>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->input('username', array('label' => __('Username'))); ?>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->input('password', array('label' => __('Password'))); ?>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->input('rePassword', array('label' => __('rePassword'), 'type' => 'password')); ?>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->input('email', array('label' => __('Email'))); ?>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->input('website', array('label' => __('Website'))); ?>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->input('phone', array('label' => __('Phone'))); ?>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <?php
                echo $this->Form->input('team_id')?>
            </div>
        </div>
        <div class="form-actions">

            <?php
            echo $this->Form->submit(
                    __('Add'), array(
                'name' => 'submit',
                'class' => 'btn btn-large btn-block btn-primary',
                'div' => false,
                'style' => 'padding: 5px 53px'));
            ?>

        </div>
    </div>
</div>
