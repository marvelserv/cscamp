<div class="row-fluid">    
    <div class="span2 offset1">
        <?php if ($loggedUserType == 'admin'): ?>
            <h3><?php echo __('Actions'); ?></h3>
            <ul class="nav nav-tabs nav-stacked">
                <li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('User.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?></li>
                <li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
            </ul>
        <?php endif; ?>
    </div>

    <div class="span8">
        <legend><?php echo __('Edit Information'); ?></legend>

        <?php echo $this->Form->create('User', array('class' => 'form-horizontal')); ?>
        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->input('name', array('label' => __('Name'))); ?>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->input('username', array('label' => __('Username'))); ?>
            </div>
        </div>
        <?php if ($loggedUserType == 'admin'): ?>
            <div class="control-group">
                <div class="controls">
                    <?php echo $this->Form->input('password', array('label' => __('Password'))); ?>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <?php echo $this->Form->input('rePassword', array('label' => __('rePassword'), 'type' => 'password')); ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->input('email', array('label' => __('Email'))); ?>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->input('website', array('label' => __('Website'))); ?>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->input('phone', array('label' => __('Phone'))); ?>
            </div>
        </div>
        <?php if ($loggedUserType == 'admin'): ?>
            <div class="control-group">
                <div class="controls">
                    <?php echo $this->Form->input('team_id') ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="form-actions">

            <?php
            echo $this->Form->submit(
                    __('Save'), array(
                'name' => 'submit',
                'class' => 'btn btn-large btn-block btn-primary',
                'div' => false,
                'style' => 'padding: 5px 53px'));
            ?>

        </div>
    </div>
</div>