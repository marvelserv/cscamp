<div class="row-fluid">
    <div class="span6 offset3">
        <legend><?php echo __('Please insert your mail to reset your password') ?></legend>
        <?php echo $this->Form->create('User'); ?>
        <div class="control-group">
            <label><?php echo __('Email address:') ?> </label>
            <div class="controls">
                <?php echo $this->Form->input('email', array('class' => 'input-xlarge', 'label' => false, 'div' => false)) ?>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->submit(__('Reset password'), array('class' => 'btn btn-primary btn-large')) ?>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
