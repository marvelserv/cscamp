<div class="row-fluid">
    <div class="span5 offset4">
        <legend><h4>Please enter your (username or Email) and password</h4></legend>
        <?php echo $this->Session->flash('auth'); ?>
        <?php echo $this->Form->create('User', array('class' => 'form-horizontal')); ?>
        <div class="control-group">
            <label class="control-label">Username</label>
            <div class="controls">
                <?php echo $this->Form->input(__('username'), array('label' => false, 'class' => 'span10')); ?>          
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Password</label>
            <div class="controls">
                <?php echo $this->Form->input(__('password'), array('label' => false, 'class' => 'span10')); ?>          
                <?php echo $this->Html->link(__('Reset password ?'), array('action' => 'forgotPassword')); ?>

                <?php echo $this->Html->link(__('New account ?'), '/'); ?> 
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <?php
                echo $this->Form->submit(__('Login'), array(
                    'name' => 'submit',
                    'class' => 'btn btn-large btn-primary',
                    'div' => false,
                    'style' => 'width: 85%',
                    ));
                ?><br />
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>