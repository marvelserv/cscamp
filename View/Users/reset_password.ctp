<div class="row-fluid">
    <div class="span6 offset3">
        <legend><?php echo __('Please, enter your new password') ?></legend>
        <?php echo $this->Form->create('User'); ?>
        <div class="control-group">
            <label><?php echo __('Password:');?> </label>
            <div class="controls">
                <?php echo $this->Form->input('password', array('class'=>'input-xlarge', 'label'=>false, 'div'=>false)) ?>
            </div>
        </div>
        <div class="control-group">
            <label><?php echo __('Password again:') ?> </label>
            <div class="controls">
                <?php echo $this->Form->input('repassword', array('type'=>'password', 'class'=>'input-xlarge', 'label'=>false, 'div'=>false)) ?>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->submit(__('Change password'), array('class'=>'btn btn-primary btn-large')) ?>
            </div>
        </div>
    </div>
</div>