<div class="row-fluid">
    <div class="span2 offset1">
        <h3><?php echo __('Actions'); ?></h3>
        <ul class="nav nav-tabs nav-stacked">
            <li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
            <li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); ?> </li>
            <li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
            <li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
        </ul>
    </div>
    <div class="span8">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <td><b><?php echo __('Id'); ?></b></td>
                    <td><?php echo h($user['User']['id']); ?></td>
                </tr>    

                <tr>    
                    <td><b><?php echo __('Name'); ?></b></td>
                    <td><?php echo h($user['User']['name']); ?></td>
                </tr>    
                <tr>
                    <td><b><?php echo __('Username'); ?></b></td>
                    <td><?php echo h($user['User']['username']); ?></td>
                </tr>    
                <tr>
                    <td><b><?php echo __('Email'); ?></b></td>
                    <td><?php echo h($user['User']['email']); ?></td>
                </tr>    
                <tr>
                    <td><b><?php echo __('WebSite'); ?></b></td>
                    <td><?php echo h($user['User']['website']); ?></td>
                </tr>  
                <tr>
                    <td><b><?php echo __('Team'); ?></b></td>
                    <td><?php echo $this->Html->link($user['Team']['name'], array('controller' => 'teams', 'action' => 'view', $user['Team']['id'])); ?></td>
                </tr>    

                <tr>
                    <td><b><?php echo __('Phone'); ?></b></td>
                    <td><?php echo h($user['User']['phone']); ?></td>
                </tr>      
                <tr>
                    <td><b><?php echo __('Role'); ?></b></td>
                    <td><?php echo h($user['User']['role']); ?></td>
                </tr>    
                <tr>
                    <td><b><?php echo __('Created'); ?></b></td>
                    <td><?php echo h($user['User']['created']); ?></td>
                </tr>    
                <tr>
                    <td><b><?php echo __('Modifies'); ?></b></td>
                    <td><?php echo h($user['User']['modified']); ?></td>

                </tr>

            </tbody>
        </table>
    </div>
</div>