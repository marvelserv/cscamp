<div class="row-fluid">
    <div class="offset1 span10">
        <h2><?php echo __('Users'); ?></h2>
        <table class="table table-bordered table-hover">
            <tr>
                <th><?php echo $this->Paginator->sort('id'); ?></th>
                <th><?php echo $this->Paginator->sort('name'); ?></th>
                <th><?php echo $this->Paginator->sort('username'); ?></th>
                
                <th><?php echo $this->Paginator->sort('email'); ?></th>
                <th><?php echo $this->Paginator->sort('phone'); ?></th>
                <th><?php echo $this->Paginator->sort('website'); ?></th>
                <th><?php echo $this->Paginator->sort('role'); ?></th>

                <th><?php echo $this->Paginator->sort('team_id'); ?></th>

                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
            <?php foreach ($users as $user): ?>
                <tr>
                    <td><?php echo h($user['User']['id']); ?>&nbsp;</td>
                    <td><?php echo h($user['User']['name']); ?>&nbsp;</td>
                    <td><?php echo h($user['User']['username']); ?>&nbsp;</td>
                    
                    <td><?php echo h($user['User']['email']); ?>&nbsp;</td>
                    <td><?php echo h($user['User']['phone']); ?>&nbsp;</td>
                    <td><?php echo h($user['User']['website']); ?>&nbsp;</td>
                    <td><?php echo h($user['User']['role']); ?>&nbsp;</td>

                    <td>
                        <?php echo $this->Html->link($user['Team']['name'], array('controller' => 'teams', 'action' => 'view', $user['Team']['id'])); ?>
                    </td>

                    <td class="actions">
                        <?php echo $this->Html->link(__('View'), array('action' => 'view', $user['User']['id'])); ?>
                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id'])); ?>
                        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
        <?php
        if ($this->Paginator->hasPage(2)) {
            echo '<div class = "well" style = "padding: 5px 20px 5px 20px !important">';
            echo $this->Paginator->prev('' . __('previous'), array(), null, array());
            echo $this->Paginator->numbers();
            echo $this->Paginator->next('' . __('next'), array(), null, array());
            echo '</div>';
        }
        ?>
    </div>
</div>

<div class="row-fluid">
    <div class="span10 offset1">
        <a href="<?php echo $this->Html->url(array("action" => "add")); ?>" class="btn btn-medium btn-primary pull-right"><?php echo __('Add new User'); ?></a>
    </div>
</div>