<div class="span10 offset1">
    <legend><?php echo __('Cscamp CTF Sponsors'); ?></legend>
    <ul class="thumbnails">
	    <?php foreach ($sponsors as $sponsor): ?>
        <li class="span4">
            <?php echo $this->Html->image('../webroot/files/sponsor/photo/' . h($sponsor['Sponsor']['id'] . '/' . $sponsor['Sponsor']['photo']), array('alt' => __('Profile Picture'), 'border' => '0', "class"=>"img-rounded", "style" => "height: 100px; width: 200px;")); ?>
            <h3><?php echo h($sponsor['Sponsor']['name']); ?></h3>
            <p><?php echo h($sponsor['Sponsor']['description']); ?></p>
            <?php if ($this->Session->read('Auth.User.role') == 'admin'): ?>
            <div class="row-fluid">
                <?php echo $this->Html->link(__('View'), array('action' => 'view', $sponsor['Sponsor']['id'])); ?>
                <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $sponsor['Sponsor']['id'])); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $sponsor['Sponsor']['id']), null, __('Are you sure you want to delete # %s?', $sponsor['Sponsor']['id'])); ?>
            </div>
            <?php endif; ?>
        </li>
        <?php endforeach; ?>
    </ul>
</div>
<?php if ($this->Session->read('Auth.User.role') == 'admin'): ?>
<div class="row-fluid">
    <div class="span10 offset1">
        <a href="<?php echo $this->Html->url(array("action" => "add")); ?>" class="btn btn-medium btn-primary pull-right"><?php echo __('Add new Sponsor'); ?></a>
    </div>
</div>
<?php endif; ?>
