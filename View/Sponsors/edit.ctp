<div class="row-fluid">
    <div class="span2 offset1">
        <ul class="nav nav-tabs nav-stacked">
		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Sponsor.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Sponsor.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Sponsors'), array('action' => 'index')); ?></li>
        </ul>
    </div>
<div class="span8">
<?php echo $this->Form->create('Sponsor', array('type' => 'file', 'class' => 'form-horizontal')); ?>
		<legend><?php echo __('Edit Sponsor'); ?></legend>
        <div class="control-group">
            <label class="control-label" for="name">Name</label>
            <div class="controls">
	<?php echo $this->Form->input('name', array("label" => false)); ?>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="description">Description</label>
            <div class="controls">
	<?php echo $this->Form->input('description', array("label" => false)); ?>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="photo">Logo</label>
            <div class="controls">
	<?php echo $this->Form->input('photo', array('type' => 'file', "label" => false)); ?>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
    <?php echo $this->Form->input('photo_dir', array('type' => 'hidden')); ?>
            </div>
        </div>
        <div class="form-actions">
            <?php
            echo $this->Form->submit(
            __('Edit'), array(
            'name' => 'submit',
            'class' => 'btn btn-large btn-block btn-primary',
            'div' => false,
            'style' => 'padding: 5px 53px'));
            ?>
</div>
</div>
</div>
