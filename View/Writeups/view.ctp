<div class="writeups view">
<h2><?php  echo __('Writeup'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($writeup['Writeup']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Team'); ?></dt>
		<dd>
			<?php echo $this->Html->link($writeup['Team']['name'], array('controller' => 'teams', 'action' => 'view', $writeup['Team']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Url'); ?></dt>
		<dd>
			<?php echo h($writeup['Writeup']['url']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($writeup['Writeup']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($writeup['Writeup']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Writeup'), array('action' => 'edit', $writeup['Writeup']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Writeup'), array('action' => 'delete', $writeup['Writeup']['id']), null, __('Are you sure you want to delete # %s?', $writeup['Writeup']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Writeups'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Writeup'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Teams'), array('controller' => 'teams', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Team'), array('controller' => 'teams', 'action' => 'add')); ?> </li>
	</ul>
</div>
