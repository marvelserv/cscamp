<div class="row-fluid">
    <div class="offset1 span10">
        <legend><?php echo __('Write-ups'); ?></legend>
        <div class="hero-unit">
            <h3>Stay tunned.</h3>
            <p>We're collecting write-ups from participated teams. If you have a write-up send it to <a href="mailto:ctf@synapse-labs.com">ctf@synapse-labs.com</a>.</p>
            <p>For updates, follow us on <a href="https://www.twitter.com/Synapse_labs">Twitter</a> or <a href="https://www.facebook.com/Synapse.Labs">Facebook</a>.</p>
        </div>
        <?php if(!empty($writeups)): ?>
        <style>
            .table th,td{
                text-align: center !important;
            }
        </style>
        <table class="table">
            <tr>
                <th><?php echo ('Team'); ?></th>
                <th><?php echo ('Country'); ?></th>
                <th><?php echo ('Score'); ?></th>
                <th><?php echo ('Write-up'); ?></th>
                <?php if ($this->Session->read('Auth.User.role') == 'admin'): ?>
                <th class="actions"><?php echo __('Actions'); ?></th>
                <?php endif; ?>
            </tr>
            <?php
            foreach ($writeups as $writeup): ?>
            <tr>
                <td>
                    <?php echo $writeup['Team']['name']; ?>
                </td>
                <td><img class="flag flag-<?php echo h($writeup['Team']['country']); ?>" />           &nbsp;</td>
                <td><?php echo h($writeup['Team']['score']); ?>&nbsp;</td>
                <td><?php echo $this->Html->link($writeup['Writeup']['url'], $writeup['Writeup']['url']); ?>&nbsp;</td>
                <?php if ($this->Session->read('Auth.User.role') == 'admin'): ?>
                <td class="actions">
                    <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $writeup['Writeup']['id'])); ?>
                    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $writeup['Writeup']['id']), null, __('Are you sure you want to delete # %s?', $writeup['Writeup']['id'])); ?>
                </td>
                <?php endif; ?>
            </tr>
            <?php endforeach; ?>
            <?php endif; ?>
        </table>
    </div>
</div>
<?php if ($this->Session->read('Auth.User.role') == 'admin'): ?>
<div class="row-fluid">
    <div class="span10 offset1">
        <a href="<?php echo $this->Html->url(array("action" => "add")); ?>" class="btn btn-medium btn-primary pull-right"><?php echo __('Add new Write-up'); ?></a>
    </div>
</div>
<?php endif; ?>
