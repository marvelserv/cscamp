<div class="row-fluid">
<div class="span5 offset1">
    <div class="well">
        <div class="row-fluid">
            <div class="span4">
                <?php
                $email = $this->Session->read('Auth.User.email');
                $trimmed = trim($email);
                $hash = md5($trimmed);
                $url = "http://www.gravatar.com/avatar/" . $hash . "?s=120";
                ?>
                <img class="thumbnail" src="<?php echo $url; ?>"/>
            </div>
            <div class="span8 pull-right">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <td><?php echo __('Username'); ?></td>
                            <td> <?php echo h($this->Session->read('Auth.User.username')) ?></td>
                        </tr>
                        <tr>
                            <td><?php echo __('Email'); ?></td>
                            <td> <?php echo h($this->Session->read('Auth.User.email')) ?></td>
                        </tr>
                        <tr>
                            <td><?php echo __('Team'); ?></td>
                            <td> <?php echo h($teamName) ?></td>
                            <td><?php echo $this->Session->read('Auth.User.Team.score'); ?></td>
                        </tr>
                        <tr>
                            <td><?php echo __('Phone'); ?></td>
                            <td> <?php echo h($this->Session->read('Auth.User.phone')) ?></td>
                        </tr>

                    </tbody>
                </table>
                <a href="<?php echo $this->Html->url(array('controller' => 'Users', 'action' => 'edit', $this->Session->read('Auth.User.id'))) ?>" class="btn btn-inverse pull-right"><i class="icon-edit icon-white"></i> <?php echo __('edit'); ?></a>
            </div>
        </div>
    </div>
</div>
<div class="span5">
        <span>Top Teams</span>
        <style>
            .flag{
                margin: 0 !important;
                margin-bottom:-15px !important;
            }
        </style>
        <table class="table table-striped">
            <tr>
                <th>Place</th>
                <th>Name</th>
                <th>Country</th>
                <th>Score</th>
            </tr>
            <?php $c = 1; foreach($topTeams as $team): ?>
            <tr>
                <td><?php echo $c; ?></td>
                <td><?php echo h($team['Team']['name']); ?></td>
                <td><img class="flag flag-<?php echo h($team['Team']['country']); ?>" />&nbsp;</td>
                <td><?php echo h($team['Team']['score']); ?></td>
            </tr>
            <?php $c++; endforeach; ?>
        </table>
    </div>
    <br />
</div>
<div class="row-fluid">
    <div class="span5 offset1">
            <style>
                .modal {width:940px; left:36%;}
            </style>
            <table class="table table-hover">
                <?php foreach($categories as $category): ?>
                <tr>
                    <th><?php echo $category['Category']['name']; ?></th>
                    <?php foreach($category['Level'] as $level): ?>
                    <?php if(in_array($level['id'], $x)): ?>
                        <!-- Button to trigger modal -->
                        <td><a style="" role="button" class="btn btn-small btn-success disabled" data-toggle="modal" href="#<?php echo $level['id'] ?>"><?php echo $level['point']?></a></td>
                    <?php else: ?>
                        <!-- Button to trigger modal -->
                        <td><a style="" role="button" class="btn btn-small btn-inverse" data-toggle="modal" href="#<?php echo $level['id'] ?>"><?php echo $level['point']?></a></td>
                    <?php endif; ?>
                    <!-- Modal -->
                    <div class="modal hide fade in" aria-hidden="true" id="<?php echo $level['id']; ?>" tabindex="-1" role="dialog">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3><?php echo $level['name']; ?></h3>
                        </div>
                        <div class="modal-body">
                            <?php
                                if (in_array($level['id'], $solvedTimes)){
                                    $allTeams = array_count_values($solvedTimes);
                                    }
                            ?>
                            <!-- Check for Web-Application Category ID -->
                            <p><?php echo $level['description']; ?></p>
                            <p>URL: <a href="<?php echo $level['url']; ?>" target="_blank"><?php echo $level['url']; ?></a></p>
                            <?php if(in_array($level['id'], $x)): ?>
                            <p>Key: <?php echo $level['key']; ?></p>
                            <?php endif; ?>
                            <?php if($level['hints']): ?>
                            <p>Hint: <?php echo $level['hints']; ?></p>
                            <?php endif; ?>
                            <p>Number of teams who solved this level is: <b>
                                <?php echo $allTeams[$level['id']]; ?></b>
                            </p>
                            <?php echo $this->Form->create('Level', array('action'=>'solveLevel', 'class'=>'form-horizontal')); ?>
                            <div class="control-group">
                                <div class="controls">
                                    <?php echo $this->Form->input('providedKey', array('id'=>'providedKey', 'div'=>false, 'label'=>false, 'placeholder'=>'Enter the key here')); ?>
                                    <?php echo $this->Form->input('team_id', array('type'=>'hidden', 'value'=>$this->Session->read('Auth.User.Team.id'))); ?>
                                    <?php echo $this->Form->input('level_id', array('type'=>'hidden', 'value'=>$level['id'])); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <div class="modal-footer">
                                        <a href="#" class="btn" data-dismiss="modal">Close</a>
                                        <button type="submit" class="btn btn-primary">Submit The Key</button>

                                    </div>
                                </div>
                            </div>
                            <?php echo $this->Form->end(); ?>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </tr>
                <?php endforeach; ?>
            </table>
    </div>
    <div class="span5" style="text-align: center;">
        <div class="accordion" id="accordion2">
            <?php foreach ($news as $new): ?>
                <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?php echo $new['News']['id'] ?>">
                            <?php echo $new['News']['title']; ?>
                        </a>
                    </div>
                    <div id="collapse<?php echo $new['News']['id'] ?>" class="accordion-body collapse">
                        <div class="accordion-inner">
                            <?php echo $new['News']['body']; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
