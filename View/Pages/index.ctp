<div class="row-fluid">
    <div class="span4 offset1">
        <a href="<?php echo $this->Html->url(array("controller" => "pages", "action" => "display", "home")); ?>"><legend><?php echo __('Cairo Security Camp CTF') ?></legend></a>

        <div id="myCarousel" class="carousel slide" style="width: 110%">
            <div class="carousel-inner">
                <div class="active item">
                    <img src="img/13.jpg" alt="">
                </div>
                <div class="item">
                    <img src="img/11.jpg" alt="">
                </div>
                <div class="item">
                    <img src="img/12.jpg" alt="">
                </div>
                <div class="item">
                    <img src="img/14.jpg" alt="">
                </div>
                <div class="item">
                    <img src="img/15.jpg" alt="">
                </div>
                <div class="item">
                    <img src="img/16.jpg" alt="">
                </div>
                <div class="item">
                    <img src="img/12.jpg" alt="">
                </div>
                <div class="item">
                    <img src="img/10.jpg" alt="">
                </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
        </div>
        <span>This year, we are planning to take the competition to more challenging levels. Competitors will get to experience both physical and mental challenges.</span>
    </div>
    <div class="span6 offset1">
        <?php echo $this->Form->create('User', array('class' => 'form-horizontal', 'url' => "/users/register")); ?>
        <legend><?php echo __('Sign up now') ?></legend>
        <div class="control-group">
            <label class="control-label" for="inputIcon"><?php echo __('Full Name'); ?></label>
            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-pencil"></i></span>
                    <?php echo $this->Form->input('name', array('id' => 'inputIcon', 'div' => false, 'label' => false, 'placeholder' => 'Full Name', 'class' => 'span10', 'style' => 'width: 116.97872340425532%')); ?>
                </div>
                <span style="color: red;">*</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputIcon"><?php echo __('Username'); ?></label>
            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-user"></i></span>
                    <?php echo $this->Form->input('username', array('id' => 'inputIcon', 'div' => false, 'label' => false, 'placeholder' => 'Username', 'class' => 'span10', 'style' => 'width: 116.97872340425532%')); ?>
                </div>
                <span style="color: red;">*</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputIcon"><?php echo __('Password'); ?></label>
            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-asterisk"></i></span>
                    <?php echo $this->Form->input('password', array('id' => 'inputIcon', 'type' => 'password', 'div' => false, 'label' => false, 'placeholder' => 'Password', 'class' => 'span10', 'style' => 'width: 116.97872340425532%')); ?>
                </div>
                <span style="color: red;">*</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputIcon"><?php echo __('Password again'); ?></label>

            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-asterisk"></i></span>
                    <?php echo $this->Form->input('rePassword', array('type' => 'password', 'id' => 'inputIcon', 'div' => false, 'label' => false, 'placeholder' => 'Password again', 'class' => 'span10', 'style' => 'width: 116.97872340425532%')); ?>
                </div>
                <span style="color: red;">*</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputIcon"><?php echo __('Email address'); ?></label>

            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-envelope"></i></span>
                    <?php echo $this->Form->input('email', array('id' => 'inputIcon', 'div' => false, 'label' => false, 'placeholder' => 'Email address', 'class' => 'span10', 'style' => 'width: 116.97872340425532%')); ?>
                </div>
                <span style="color: red;">*</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputIcon"><?php echo __('Web-Site'); ?></label>
            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class=" icon-globe"></i></span>
                    <?php echo $this->Form->input('website', array('id' => 'inputIcon', 'div' => false, 'label' => false, 'placeholder' => 'Web Site', 'class' => 'span10', 'style' => 'width: 116.97872340425532%')); ?>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputIcon"><?php echo __('Phone'); ?></label>
            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-signal"></i></span>
                    <?php echo $this->Form->input('phone', array('id' => 'inputIcon', 'div' => false, 'label' => false, 'placeholder' => 'Cell phone number', 'class' => 'span10', 'style' => 'width: 116.97872340425532%')); ?>
                </div>
            </div>

        </div>
        <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn"><?php echo __('Sign up'); ?></button>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<br />
<div class="row-fluid">
    <div class="span4 offset1">
        <a href="<?php echo $this->Html->url(array("controller" => "teams", "action" => "teamsList")); ?>" class="btn btn-large btn-primary" style="width: 100%; padding: 20px;"><?php echo __('List of registered teams') ?></a>
    </div>
    <div class="span6 offset1">

        <table class="table table-hover" style="border-style: hidden">
            <tr>
                <th>Information</th>
                <td>Questions about the CScamp CTF should be sent to <a href="mailto:ctf@synapse-labs.com">ctf@synapse-labs.com</a>.</td>
            </tr>
            <tr>
                <th>IRC</th>
                <td>Join <span style="color: maroon">#synapse-ctf</span> channel on irc.freenode.com port 6697. Use (<span style="color: maroon">SSL</span>).</td>
            </tr>
        </table>
    </div>
</div>
