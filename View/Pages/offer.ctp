
<div class="span10 offset1">
    <div class="hero-unit">
        <div class="row-fluid">
            <div class="span8 offset2">
                <h2>Welcome To Cairo Security Camp CTF 2012.</h2>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span8 offset2">
                <p>You've to Join a Team or create a new one in order to participate in the CTF.<p>
            </div>
        </div>
        <div class="row-fluid">
            <br />
        </div>
        <div class="row-fluid">
            <div class="span4 offset4">
                <a style="" class="btn btn-large btn-primary" data-toggle="modal" href="#myModal">Join a Team</a>
                <div class="modal hide" id="myModal">
                    <div class="modal-body">

                        <legend>You've to provide me with the team key…</legend>
                        <?php echo $this->Form->create('User', array('action'=>'joinTeam', 'class'=>'form-horizontal')); ?>
                        <div class="control-group">
                            <label class="control-label" for="Team">Team</label>
                            <div class="controls">
                                <?php echo $this->Form->input('team_id', array('options'=>$teams, 'id'=>'Team', 'div'=>false, 'label'=>false)); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="Key">Key</label>
                            <div class="controls">
                                <?php echo $this->Form->input('key', array('label' => false, 'div'=> false, 'id'=>'key')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="controls">
                                <div class="modal-footer">
                                    <a href="#" class="btn" data-dismiss="modal">Close</a>
                                    <button type="submit" class="btn btn-primary">Save changes</button>

                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>

                    </div>
                </div>

                <a style="" class="btn btn-large btn-primary" data-toggle="modal" href="#myModal2">Create a Team</a>
                <div class="modal hide" id="myModal2">
                    <div class="modal-body">

                        <legend>Set the team name and the key…</legend>
                        <?php echo $this->Form->create('Team', array('action'=>'add', 'class'=>'form-horizontal')); ?>
                        <div class="control-group">
                            <label class="control-label" for="Team">Team</label>
                            <div class="controls">
                                <?php echo $this->Form->input('name', array('id'=>'Team', 'div'=>false, 'label'=>false)); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="Key">Key</label>
                            <div class="controls">
                                <?php echo $this->Form->input('secretkey', array('label' => false, 'div'=> false, 'id'=>'key')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="Country">Country</label>
                            <div class="controls">
                                <?php
                                $countries = array(
                                'Countries'=>array(
                                'af' =>    'Afganistan', 
        'al' =>    'Albania', 
        'dz' =>    'Algeria', 
        'as' => 'American Samoa', 
        'ad' => 'Andorra', 
        'ao' => 'Angola', 
        'ai' => 'Anguilla', 
        'aq' => 'Antarctica', 
        'ag' => 'Antigua and Barbuda', 
        'ar' => 'Argentina', 
        'am' => 'Armenia', 
        'aw' => 'Aruba', 
        'au' => 'Australia', 
        'at' => 'Austria', 
        'az' => 'Azerbaijan', 
        'bs' => 'Bahamas', 
        'bh' => 'Bahrain', 
        'bd' => 'Bangladesh', 
        'bb' => 'Barbados', 
        'by' => 'Belarus', 
        'be' => 'Belgium', 
        'bz' => 'Belize', 
        'bj' => 'Benin', 
        'bm' => 'Bermuda', 
        'bt' => 'Bhutan', 
        'bo' => 'Bolivia', 
        'ba' => 'Bosnia and Herzegowina', 
        'bw' => 'Botswana', 
        'bv' => 'Bouvet Island', 
        'br' => 'Brazil', 
        'io' => 'British Indian Ocean Territory', 
        'bn' => 'Brunei Darussalam', 
        'bg' => 'Bulgaria', 
        'bf' => 'Burkina Faso', 
        'bi' => 'Burundi', 
        'kh' => 'Cambodia', 
        'cm' => 'Cameroon', 
        'ca' => 'Canada', 
        'cv' => 'Cape Verde', 
        'ky' => 'Cayman Islands', 
        'cf' => 'Central African Republic', 
        'td' => 'Chad', 
        'cl' => 'Chile', 
        'cn' => 'China', 
        'cx' => 'Christmas Island', 
        'cc' => 'Cocos Keeling Islands', 
        'co' => 'Colombia', 
        'km' => 'Comoros', 
        'cg' => 'Congo', 
        'cd' => 'Congo, Democratic Republic of the', 
        'ck' => 'Cook Islands', 
        'cr' => 'Costa Rica', 
        'ci' => 'Cote d\'Ivoire', 
        'hr' => 'Croatia Hrvatska', 
        'cu' => 'Cuba', 
        'cy' => 'Cyprus', 
        'cz' => 'Czech Republic', 
        'dk' => 'Denmark', 
        'dj' => 'Djibouti', 
        'dm' => 'Dominica', 
        'do' => 'Dominican Republic', 
        'tp' => 'East Timor', 
        'ec' => 'Ecuador', 
        'eg' => 'Egypt', 
        'sv' => 'El Salvador', 
        'gq' => 'Equatorial Guinea', 
        'er' => 'Eritrea', 
        'ee' => 'Estonia', 
        'et' => 'Ethiopia', 
        'fk' => 'Falkland Islands Malvinas', 
        'fo' => 'Faroe Islands', 
        'fj' => 'Fiji', 
        'fi' => 'Finland', 
        'fr' => 'France', 
        'fx' => 'France, Metropolitan', 
        'gf' => 'French Guiana', 
        'pf' => 'French Polynesia', 
        'tf' => 'French Southern Territories', 
        'ga' => 'Gabon', 
        'gm' => 'Gambia', 
        'ge' => 'Georgia', 
        'de' => 'Germany', 
        'gh' => 'Ghana', 
        'gi' => 'Gibraltar', 
        'gr' => 'Greece', 
        'gl' => 'Greenland', 
        'gd' => 'Grenada', 
        'gp' => 'Guadeloupe', 
        'gu' => 'Guam', 
        'gt' => 'Guatemala', 
        'gn' => 'Guinea', 
        'gw' => 'Guinea-Bissau', 
        'gy' => 'Guyana', 
        'ht' => 'Haiti', 
        'hm' => 'Heard and Mc Donald Islands', 
        'va' => 'Holy See (Vatican City State)', 
        'hn' => 'Honduras', 
        'hk' => 'Hong Kong', 
        'hu' => 'Hungary', 
        'is' => 'Iceland', 
        'in' => 'India', 
        'id' => 'Indonesia', 
        'ir' => 'Iran, Islamic Republic of', 
        'iq' => 'Iraq', 
        'ie' => 'Ireland', 
        'il' => 'Israel', 
        'it' => 'Italy', 
        'hm' => 'Jamaica', 
        'jp' => 'Japan', 
        'jo' => 'Jordan', 
        'kz' => 'Kazakhstan', 
        'ke' => 'Kenya', 
        'ki' => 'Kiribati', 
        'kp' => 'Korea, Democratic People\'s Republic of', 
        'kr' => 'Korea, Republic of', 
        'kw' => 'Kuwait', 
        'kg' => 'Kyrgyzstan', 
        'la' => 'Lao People\'s Democratic Republic', 
        'lv' => 'Latvia', 
        'lb' => 'Lebanon', 
        'ls' => 'Lesotho', 
        'lr' => 'Liberia', 
        'ly' => 'Libyan Arab Jamahiriya', 
        'li' => 'Liechtenstein', 
        'lt' => 'Lithuania', 
        'lu' => 'Luxembourg', 
        'mo' => 'Macau', 
        'mk' => 'Macedonia, The Former Yugoslav Republic of', 
        'mg' => 'Madagascar', 
        'mw' => 'Malawi', 
        'my' => 'Malaysia', 
        'mv' => 'Maldives', 
        'ml' => 'Mali', 
        'mt' => 'Malta', 
        'mh' => 'Marshall Islands', 
        'mq' => 'Martinique', 
        'mr' => 'Mauritania', 
        'mu' => 'Mauritius', 
        'yt' => 'Mayotte', 
        'mx' => 'Mexico', 
        'fm' => 'Micronesia, Federated States of', 
        'md' => 'Moldova, Republic of', 
        'mc' => 'Monaco', 
        'mn' => 'Mongolia', 
        'ms' => 'Montserrat', 
        'ma' => 'Morocco', 
        'mz' => 'Mozambique', 
        'mm' => 'Myanmar', 
        'na' => 'Namibia', 
        'nr' => 'Nauru', 
        'np' => 'Nepal', 
        'nl' => 'Netherlands', 
        'an' => 'Netherlands Antilles', 
        'nc' => 'New Caledonia', 
        'nz' => 'New Zealand', 
        'ni' => 'Nicaragua', 
        'ne' => 'Niger', 
        'ng' => 'Nigeria', 
        'nu' => 'Niue', 
        'nf' => 'Norfolk Island', 
        'mp' => 'Northern Mariana Islands', 
        'no' => 'Norway', 
        'om' => 'Oman', 
        'pk' => 'Pakistan', 
        'pw' => 'Palau', 
        'pa' => 'Panama', 
        'pg' => 'Papua New Guinea', 
        'py' => 'Paraguay', 
        'pe' => 'Peru', 
        'ph' => 'Philippines', 
        'pn' => 'Pitcairn', 
        'pl' => 'Poland', 
        'pt' => 'Portugal', 
        'pr' => 'Puerto Rico', 
        'qa' => 'Qatar', 
        're' => 'Reunion', 
        'ro' => 'Romania', 
        'ru' => 'Russian Federation', 
        'rw' => 'Rwanda', 
        'kn' => 'Saint Kitts and Nevis', 
        'lc' => 'Saint LUCIA', 
        'vc' => 'Saint Vincent and the Grenadines', 
        'ws' => 'Samoa', 
        'sm' => 'San Marino', 
        'st' => 'Sao Tome and Principe', 
        'sa' => 'Saudi Arabia', 
        'sn' => 'Senegal', 
        'sc' => 'Seychelles', 
        'sl' => 'Sierra Leone', 
        'sg' => 'Singapore', 
        'sk' => 'Slovakia (Slovak Republic)', 
        'si' => 'Slovenia', 
        'sb' => 'Solomon Islands', 
        'so' => 'Somalia', 
        'za' => 'South Africa', 
        'gs' => 'South Georgia and the South Sandwich Islands', 
        'es' => 'Spain', 
        'lk' => 'Sri Lanka', 
        'sh' => 'St. Helena', 
        'pm' => 'St. Pierre and Miquelon', 
        'sd' => 'Sudan', 
        'sr' => 'Suriname', 
        'sj' => 'Svalbard and Jan Mayen Islands', 
        'sz' => 'Swaziland', 
        'se' => 'Sweden', 
        'ch' => 'Switzerland', 
        'sy' => 'Syrian Arab Republic', 
        'tw' => 'Taiwan, Province of China', 
        'tj' => 'Tajikistan', 
        'tz' => 'Tanzania, United Republic of', 
        'th' => 'Thailand', 
        'tg' => 'Togo', 
        'tk' => 'Tokelau', 
        'to' => 'Tonga', 
        'tt' => 'Trinidad and Tobago', 
        'tn' => 'Tunisia', 
        'tr' => 'Turkey', 
        'tm' => 'Turkmenistan', 
        'tc' => 'Turks and Caicos Islands', 
        'tv' => 'Tuvalu', 
        'ug' => 'Uganda', 
        'ua' => 'Ukraine', 
        'ae' => 'United Arab Emirates', 
        'gb' => 'United Kingdom', 
        'us' => 'United States', 
        'um' => 'United States Minor Outlying Islands', 
        'uy' => 'Uruguay', 
        'uz' => 'Uzbekistan', 
        'vu' => 'Vanuatu', 
        've' => 'Venezuela', 
        'vn' => 'Viet Nam', 
        'vg' => 'Virgin Islands (British)', 
        'vi' => 'Virgin Islands (U.S.)', 
        'wf' => 'Wallis and Futuna Islands', 
        'eh' => 'Western Sahara', 
        'ye' => 'Yemen', 
        'yu' => 'Yugoslavia', 
        'zm' => 'Zambia', 
        'zw' => 'Zimbabwe' 
                                ));
                                ?>
                                <?php echo $this->Form->select('country', $countries, array('label' => false, 'div'=> false, 'id'=>'country')); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="controls">
                                <div class="modal-footer">
                                    <a href="#" class="btn" data-dismiss="modal">Close</a>
                                    <button type="submit" class="btn btn-primary">Save changes</button>

                                </div>
                            </div>
                        </div>

                        <?php echo $this->Form->end(); ?>

                    </div>
                </div>

            </div>
        </div>
        <div class="row-fluid">
            <div class="span4 offset4">
                <br />
                <a class="btn btn-large btn-primary" style="width: 73%" href="rules">Check the rules</a>
            </div>
        </div>
    </div>
</div>
