<div class="abouts form">
<?php echo $this->Form->create('About', array('type'=>'file')); ?>
	<fieldset>
		<legend><?php echo __('Add About'); ?></legend>
	<?php
		echo $this->Form->input('photo', array('type'=>'file'));
		echo $this->Form->input('photo_dir', array('type'=>'hidden'));
		echo $this->Form->input('description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Abouts'), array('action' => 'index')); ?></li>
	</ul>
</div>
