<div class="row-fluid">
    <div class="offset1 span10">
        <legend><?php echo __('Levels'); ?></legend>
        <table class="table table-bordered table-hover">
            <tr>
                <th><?php echo $this->Paginator->sort('id'); ?></th>
                <th><?php echo $this->Paginator->sort('name'); ?></th>
                <th><?php echo $this->Paginator->sort('description'); ?></th>
                <th><?php echo $this->Paginator->sort('url'); ?></th>
                <th><?php echo $this->Paginator->sort('point'); ?></th>
                <th><?php echo $this->Paginator->sort('key'); ?></th>
                <th><?php echo $this->Paginator->sort('hints'); ?></th>
                <th><?php echo $this->Paginator->sort('category_id'); ?></th>

                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
            <?php foreach ($levels as $level): ?>
                <tr>
                    <td><?php echo h($level['Level']['id']); ?>&nbsp;</td>
                    <td><?php echo h($level['Level']['name']); ?>&nbsp;</td>
                    <td><?php echo h($level['Level']['description']); ?>&nbsp;</td>
                    <td><?php echo $this->Html->link(h($level['Level']['url'])); ?>&nbsp;</td>
                    <td><?php echo h($level['Level']['point']); ?>&nbsp;</td>
                    <td><?php echo h($level['Level']['key']); ?>&nbsp;</td>
                    <td><?php echo h($level['Level']['hints']); ?>&nbsp;</td>
                    <td>
                        <?php echo $this->Html->link($level['Category']['name'], array('controller' => 'categories', 'action' => 'view', $level['Category']['id'])); ?>
                    </td>

                    <td class="actions">
                        <?php echo $this->Html->link(__('View'), array('action' => 'view', $level['Level']['id'])); ?>
                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $level['Level']['id'])); ?>
                        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $level['Level']['id']), null, __('Are you sure you want to delete # %s?', $level['Level']['id'])); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
        <?php
        if ($this->Paginator->hasPage(2)) {
            echo '<div class = "well" style = "padding: 5px 20px 5px 20px !important">';
            echo $this->Paginator->prev('' . __('previous'), array(), null, array());
            echo $this->Paginator->numbers();
            echo $this->Paginator->next('' . __('next'), array(), null, array());
            echo '</div>';
        }
        ?>
    </div>
</div>

<div class="row-fluid">
    <div class="span10 offset1">
        <a href="<?php echo $this->Html->url(array("action" => "add")); ?>" class="btn btn-medium btn-primary pull-right"><?php echo __('Add new Level'); ?></a>
    </div>
</div>
