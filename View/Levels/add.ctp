<div class="row_fluid">
    <div class="span2 offset1">
        <ul class="nav nav-tabs nav-stacked">

            <li><?php echo $this->Html->link(__('List Levels'), array('action' => 'index')); ?></li>
            <li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
            <li><?php echo $this->Html->link(__('New Category'), array('controller' => 'categories', 'action' => 'add')); ?> </li>
        </ul>
    </div>
    <div class="span8">

        <legend><?php echo __('Add Level'); ?></legend>
        <?php echo $this->Form->create('Level', array('class' => 'form-horizontal')); ?>
        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->input('name'); ?>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->input('description'); ?>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->input('url'); ?>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->input('point'); ?>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->input('key'); ?>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->input('hints'); ?> 
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->input('category_id');?>
            </div>
        </div>

        <div class="form-actions">

            <?php
            echo $this->Form->submit(
            __('Add'), array(
            'name' => 'submit',
            'class' => 'btn btn-large btn-block btn-primary',
            'div' => false,
            'style' => 'padding: 5px 53px'));
            ?>

        </div>
    </div>
</div>
