<div class="row-fluid">
    <div class="span2 offset1">
        <h3><?php echo __('Actions'); ?></h3>
        <ul class="nav nav-tabs nav-stacked">
            <li><?php echo $this->Html->link(__('Add News'), array('action' => 'add')); ?></li>

        </ul>
    </div>
    <div class="span8">
        <h2><?php echo __('News'); ?></h2>
        <table class="table table-bordered table-striped">
            <tr>
                <th><?php echo $this->Paginator->sort('id'); ?></th>
                <th><?php echo $this->Paginator->sort('Title'); ?></th>
                <th><?php echo $this->Paginator->sort('News'); ?></th>

                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
            <?php foreach ($news as $new): ?>
                <tr>
                    <td><?php echo h($new['News']['id']); ?>&nbsp;</td>
                    <td><?php echo h($new['News']['title']); ?>&nbsp;</td>
                    <td><?php echo h($new['News']['body']); ?>&nbsp;</td>


                    <td class="actions">
                        <?php echo $this->Html->link(__('View'), array('action' => 'view', $new['News']['id']), array('class' => 'btn btn-inverse btn-small')); ?>
                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $new['News']['id']), array('class' => 'btn btn-inverse btn-small')); ?>
                        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $new['News']['id']), array('class' => 'btn btn-danger btn-small'), __('Are you sure you want to delete # %s?', $new['News']['id'])); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
        <?php
        if ($this->Paginator->hasPage(2)) {
            echo '<div class = "well" style = "padding: 5px 20px 5px 20px !important">';
            echo $this->Paginator->prev('' . __('previous'), array(), null, array());
            echo $this->Paginator->numbers();
            echo $this->Paginator->next('' . __('next'), array(), null, array());
            echo '</div>';
        }
        ?>
    </div>
</div>
