<div class="row-fluid">
    <div class="span2 offset1">
        <h3><?php echo __('Actions'); ?></h3>
        <ul class="nav nav-tabs nav-stacked">
<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('News.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('News.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List News'), array('action' => 'index')); ?></li>
        </ul>  
    </div>
    <div class="span8">
        <legend><?php echo __('Edit New'); ?></legend>
        <?php echo $this->Form->create('News', array('class' => 'form-horizontal')); ?>

        <div class="control-group">
            <div class="controls">
                <?php echo $this->Form->input('title', array('label'=> __('Title'))); ?>
                <?php echo $this->Form->input('body', array('label'=> __('New'))); ?>
            </div>
        </div>

        <div class="form-actions">

            <?php
            echo $this->Form->submit(
                    __('Edit New'), array(
                'name' => 'submit',
                'class' => 'btn btn-large btn-block btn-primary',
                'div' => false,
                'style' => 'padding: 5px 28px'));
            ?>

        </div>
    </div>
</div>
