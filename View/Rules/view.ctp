<div class="row-fluid">
        <div class="span2 offset1">
        <h3><?php echo __('Actions'); ?></h3>
        <ul class="nav nav-tabs nav-stacked">
            <li><?php echo $this->Html->link(__('Edit Rule'), array('action' => 'edit', $rule['Rule']['id'])); ?> </li>
            <li><?php echo $this->Form->postLink(__('Delete Rule'), array('action' => 'delete', $rule['Rule']['id']), null, __('Are you sure you want to delete # %s?', $rule['Rule']['id'])); ?> </li>
            <li><?php echo $this->Html->link(__('List Rules'), array('action' => 'index')); ?> </li>
            <li><?php echo $this->Html->link(__('New Rule'), array('action' => 'add')); ?> </li>
        </ul>
    </div>
<div class="span8">
    <legend>The Rule</legend>
    <table class="table table-bordered table-striped">
        <tbody>
	
		<tr>
                    <td><?php echo __('Id'); ?></td>
		<td>
			<?php echo h($rule['Rule']['id']); ?>
			&nbsp;
		</td>
                </tr>
		<tr>
                    <td><?php echo __('Rule'); ?></td>
		<td>
			<?php echo h($rule['Rule']['rule']); ?>
			&nbsp;
		</td>
                </tr>
		<tr>
                    <td><?php echo __('Maxplayers'); ?></td>
		<td>
			<?php echo h($rule['Rule']['maxplayers']); ?>
			&nbsp;
		</td>
                </tr>
		<tr>
                    <td><?php echo __('Minplayers'); ?></td>
		<td>
			<?php echo h($rule['Rule']['minplayers']); ?>
			&nbsp;
		</td>
                </tr>
	
        </tbody>
    </table>
</div>

</div>
