<div class="row_fluid">
    <div class="span2 offset1">
        <ul class="nav nav-tabs nav-stacked">


            <li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Rule.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Rule.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Rules'), array('action' => 'index')); ?></li>
        </ul>
    </div>
</div>
<div class="span7">
    <?php echo $this->Form->create('Rule', array('class' => 'form-horizontal')); ?>

    <legend><?php echo __('Edit Rule'); ?></legend>
    <div class="control-group">
        <div class="controls">
            <?php
            echo $this->Form->input('rule');
            ?>
        </div>
    </div>

    <div class="form-actions">

        <?php
        echo $this->Form->submit(
                __('Add'), array(
            'name' => 'submit',
            'class' => 'btn btn-large btn-block btn-primary',
            'div' => false,
            'style' => 'padding: 5px 53px'));
        ?>

    </div>
</div>