<div class="row-fluid">
    <div class="offset1 span10">
        <legend><h3><?php echo __('Rules'); ?></h3></legend>
        <dl class="dl-horizontal">




            <?php
            $c = 1;
            foreach ($rules as $rule):
                ?>

                <dt><?php echo 'rule'."#{$c}"; ?></dt>
                <dd><?php echo h( $rule['Rule']['rule']); ?>&nbsp;</dd>
                <?php if ($this->Session->read('Auth.User.role') == 'admin'): ?>
                <dd class="actions">
                    <?php echo $this->Html->link(__('View'), array('action' => 'view', $rule['Rule']['id']), array('class' => 'btn btn-small btn-inverse')); ?>
                    <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $rule['Rule']['id']), array('class' => 'btn btn-small btn-inverse')); ?>
                    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $rule['Rule']['id']), array('class' => 'btn btn-small btn-danger'), __('Are you sure you want to delete # %s?', $rule['Rule']['id'])); ?>
                </dd>
                <?php endif; ?>
            <?php $c++; endforeach; ?>
        </dl>
        <p>
            <?php
            if ($this->Paginator->hasPage(2)) {
                echo '<div class = "well" style = "padding: 5px 20px 5px 20px !important">';
                echo $this->Paginator->prev('' . __('previous'), array(), null, array());
                echo $this->Paginator->numbers();
                echo $this->Paginator->next('' . __('next'), array(), null, array());
                echo '</div>';
            }
            ?>

    </div>
</div>
<?php if ($this->Session->read('Auth.User.role') == 'admin'): ?>
<div class="row-fluid">
    <div class="span2 offset1">
        <a href="<?php echo $this->Html->url(array("action" => "add")); ?>" class="btn btn-medium btn-primary"><?php echo __('Add new Rule'); ?></a>
    </div>
</div>
<?php endif; ?>