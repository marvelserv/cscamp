<div class="row-fluid">
    <div class="span10 offset1">
        <div class="btn-group <?php echo __('pull-right'); ?>">
            <?php echo $this->Html->link(__('Edit Team'), array('action' => 'edit', $team['Team']['id']), array('class' => 'btn btn-inverse')); ?>
            <?php echo $this->Form->postLink(__('Delete Team'), array('action' => 'delete', $team['Team']['id']), array('class' => 'btn btn-inverse'), null, __('Are you sure you want to delete # %s?', $team['Team']['id'])); ?>
            <?php echo $this->Html->link(__('List Teams'), array('action' => 'index'), array('class' => 'btn btn-inverse')); ?>
            <?php echo $this->Html->link(__('New Team'), array('action' => 'add'), array('class' => 'btn btn-inverse')); ?>
            <?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index'), array('class' => 'btn btn-inverse')); ?>
            <?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add'), array('class' => 'btn btn-inverse')); ?>
        </div>
        <legend><?php  echo __('Team'); ?></legend>
        <style>
            .flag{
                margin: 0 !important;
                margin-bottom:-15px !important;
            }
        </style>
        <table class="table table-bordered table-striped">
            <tr><th><?php echo __('Id'); ?></th>
            <td>
                <?php echo h($team['Team']['id']); ?>
                &nbsp;</td>
            </tr>
            <tr><th><?php echo __('Name'); ?></th>
            <td>
                <?php echo h($team['Team']['name']); ?>
                &nbsp;</td>
            </tr>
            <tr><th><?php echo __('Country'); ?></th>
            <td>
                <img class="flag flag-<?php echo h($team['Team']['country']); ?>" />           &nbsp;</td>
            </tr>
            <tr><th><?php echo __('Score'); ?></th>
            <td>
                <?php echo h($team['Team']['score']); ?>
                &nbsp;</td>
            </tr>
            <tr><th><?php echo __('Secretkey'); ?></th>
            <td>
                <?php echo h($team['Team']['secretkey']); ?>
                &nbsp;</td>
            </tr>
            <tr><th><?php echo __('Created'); ?></th>
            <td>
                <?php echo h($team['Team']['created']); ?>
                &nbsp;</td>
            </tr>
            <tr><th><?php echo __('Modified'); ?></th>
            <td>
                <?php echo h($team['Team']['modified']); ?>
                &nbsp;</td>
            </tr>
	    </table>
    </div>
</div>
<div class="row-fluid">
    <div class="span10 offset1">
	<legend><?php echo __('Related Users'); ?></legend>
	<?php if (!empty($team['User'])): ?>
    <table class="table table-striped">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Username'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('Website'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($team['User'] as $user): ?>
		<tr>
			<td><?php echo $user['id']; ?></td>
			<td><?php echo $user['name']; ?></td>
			<td><?php echo $user['username']; ?></td>
			<td><?php echo $user['email']; ?></td>
			<td><?php echo $user['website']; ?></td>
			<td><?php echo $user['created']; ?></td>
			<td><?php echo $user['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'users', 'action' => 'view', $user['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'users', 'action' => 'edit', $user['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'users', 'action' => 'delete', $user['id']), null, __('Are you sure you want to delete # %s?', $user['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
        <?php if ($this->Paginator->hasPage(2)): ?>
            <div class="well">
                <?php
                echo $this->Paginator->prev(__('previous '), array(), null, array('class' => 'prev disabled'));
                echo ' | ';
                echo $this->Paginator->next(__('next '), array(), null, array('class' => 'next disabled'));
                ?>
            </div>
        <?php endif; ?>
<?php endif; ?>
</div>
</div>
