<div class="row-fluid">
    <div class="offset1 span10">
        <legend style="text-align: center;"><h2><?php echo __('Teams Ordered By Score'); ?></h2></legend>
        <style>
            td,th{
                text-align:center !important;
            }
        </style>
        <table class="table table-bordered table-striped">
            <tr class="success">
                <th></th>
                <th><?php echo 'Place'; ?></th>
                <th><?php echo 'name'; ?></th>
                <th><?php echo 'Country'; ?></th>
                <th><?php echo 'score'; ?></th>
                <th><?php echo 'created'; ?></th>
                <?php if ($this->Session->read('Auth.User.role') == 'admin'): ?>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                <?php endif; ?>
            </tr>

            <?php $c = 0; foreach ($teams as $team): ?>
            <?php if($c == 0): ?>
            <tr class="success">
            <?php else: ?>
            <tr>
            <?php endif; ?>
            <?php $c++; ?>
            <?php if($c == 1): ?>
                <td><img src="img/crown.gif" style="width: 20px; height: 17px;" /></td>
                <?php else: ?>
                <td></td>
                <?php endif; ?>
                <td><?php echo h($c); ?>&nbsp;</td>
                <td><?php echo h($team['Team']['name']); ?>&nbsp;</td>
                <td><img class="flag flag-<?php echo h($team['Team']['country']); ?>" />&nbsp;</td>
                <td><?php echo h($team['Team']['score']); ?>&nbsp;</td>
                <td><?php echo h($team['Team']['created']); ?>&nbsp;</td>
                <?php if ($this->Session->read('Auth.User.role') == 'admin'): ?>
                <td class="actions">
                    <?php echo $this->Html->link(__('View'), array('action' => 'view', $team['Team']['id'])); ?>
                    <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $team['Team']['id'])); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $team['Team']['id']), null, __('Are you sure you want to delete "%s"?', $team['Team']['name'])); ?>
                </td>
            <?php endif; ?>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php
        if ($this->Paginator->hasPage(2)) {
            echo '<div class = "well" style = "padding: 5px 20px 5px 20px !important">';
            echo $this->Paginator->prev('' . __('previous'), array(), null, array());
            echo $this->Paginator->numbers();
            echo $this->Paginator->next('' . __('next'), array(), null, array());
            echo '</div>';
        }
        ?>

    </div>
<?php if ($this->Session->read('Auth.User.role') == 'admin'): ?>
        <div class="row-fluid">
            <div class="span10 offset1">
                <a href="<?php echo $this->Html->url(array("action" => "add")); ?>" class="btn btn-medium btn-primary pull-right"><?php echo __('Add new Team'); ?></a>
            </div>
        </div>
<?php endif; ?>
</div>
