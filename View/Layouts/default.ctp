<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo __('CScamp: '); ?>
            <?php echo $title_for_layout; ?>
        </title>
        <?php
        echo $this->Html->meta('icon', $this->Html->url('/icon.ico'));
        echo $this->Html->css(__('bootstrap'));
        echo $this->Html->css(__('flags'));
        echo $this->Html->css(__('prettify'));
        echo $this->Html->script('jq');
        echo $this->Html->script('bootsratp');
        echo $this->Html->script('bootstrap-transition');
        echo $this->Html->script('bootstrap-dropdown');
        echo $this->Html->script('bootstrap-collapse');
        echo $this->Html->script('carousel');
        echo $this->Html->script('bootstrap-modal');
        echo $this->Html->script('prettify');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>
    </head>
    <body onload="prettyPrint()">
        <div class="container-fluid">
            <div class="navbar navbar-inverse">
                <div class="navbar-inner">
                    <?php echo $this->Html->link(__('CScamp'), array('controller' => 'pages', 'action' => 'display', 'home'), array("class" => 'brand')) ?>

                    <?php if ($this->Session->read('Auth.User.id')): ?>
                    <?php if ($this->Session->read('Auth.User.role') == 'admin'): ?>
                        <ul class="nav">
                            <li class="dropdown">
                                <a class="dropdown-toggle"
                                   data-toggle="dropdown"
                                   href="#">
                                       <?php echo __('Categories'); ?>
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                                    <li><?php echo $this->Html->link(__('Add Category'), array('controller' => 'categories', 'action' => 'add')); ?></li>
                                    <li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index')); ?></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a class="dropdown-toggle"
                                   data-toggle="dropdown"
                                   href="#">
                                       <?php echo __('Levels'); ?>
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                                    <li><?php echo $this->Html->link(__('Add Level'), array('controller' => 'levels', 'action' => 'add')); ?></li>
                                    <li><?php echo $this->Html->link(__('List Levels'), array('controller' => 'levels', 'action' => 'index')); ?></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav">
                            <li class="dropdown">
                                <a class="dropdown-toggle"
                                   data-toggle="dropdown"
                                   href="#">
                                       <?php echo __('Users'); ?>
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                                    <li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?></li>
                                    <li><?php echo $this->Html->link(__('Add User'), array('controller' => 'users', 'action' => 'add')); ?></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav">
                            <li class="dropdown">
                                <a class="dropdown-toggle"
                                   data-toggle="dropdown"
                                   href="#">
                                       <?php echo __('Teams'); ?>
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                                    <li><?php echo $this->Html->link(__('List Teams'), array('controller' => 'teams', 'action' => 'index')); ?></li>
                                    <li><?php echo $this->Html->link(__('Add Team'), array('controller' => 'teams', 'action' => 'add')); ?></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav">
                            <li class="dropdown">
                                <a class="dropdown-toggle"
                                   data-toggle="dropdown"
                                   href="#">
                                       <?php echo __('Rules'); ?>
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                                    <li><?php echo $this->Html->link(__('List Rules'), array('controller' => 'rules', 'action' => 'index')); ?></li>
                                    <li><?php echo $this->Html->link(__('Add Rule'), array('controller' => 'rules', 'action' => 'add')); ?></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav">
                        <ul class="nav">
                            <li class="dropdown">
                                <a class="dropdown-toggle"
                                   data-toggle="dropdown"
                                   href="#">
                                       <?php echo __('Sponsors'); ?>
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                                    <li><?php echo $this->Html->link(__('List Sponsors'), array('controller' => 'Sponsors', 'action' => 'index')); ?></li>
                                    <li><?php echo $this->Html->link(__('Add Sponsor'), array('controller' => 'Sponsors', 'action' => 'add')); ?></li>
                                </ul>
                            </li>
                        </ul>
                            <li class="dropdown">
                                <a class="dropdown-toggle"
                                   data-toggle="dropdown"
                                   href="#">
                                       <?php echo __('News'); ?>
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                                    <li><?php echo $this->Html->link(__('List news'), array('controller' => 'News', 'action' => 'index')); ?></li>
                                    <li><?php echo $this->Html->link(__('Add New'), array('controller' => 'News', 'action' => 'add')); ?></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav">
                            <li class="dropdown">
                                <a class="dropdown-toggle"
                                   data-toggle="dropdown"
                                   href="#">
                                       <?php echo __('Write-ups'); ?>
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                                    <li><?php echo $this->Html->link(__('List Write-ups'), array('controller' => 'writeups', 'action' => 'index')); ?></li>
                                    <li><?php echo $this->Html->link(__('Add Write-up'), array('controller' => 'writeups', 'action' => 'add')); ?></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav">
                            <li class="<?php
                    if ($this->params['controller'] == "TeamsLevels") {
                        echo "active";
                    }
                        ?>"><?php echo $this->Html->link(__('statistics'), array('controller' => 'TeamsLevels', 'action' => 'index')) ?></li>
                        </ul>
                        <p style="margin-top: 10px;" class="<?php echo __('pull-right') ?>"><?php echo __("You're logged in as") ?> <?php echo h($this->Session->read('Auth.User.username')); ?> <?php echo $this->Html->link(__('logout'), array('controller' => 'users', 'action' => 'logout')) ?></p>
                        <?php else: ?>
                        <ul class="nav">

                            <li class="<?php
                    if ($this->params['controller'] == "Teams") {
                        echo "active";
                    }
                        ?>"><?php echo $this->Html->link(__('Scoreboard'), array('controller' => 'Teams', 'action' => 'index')) ?></li>
                            <li class="<?php
                    if ($this->params['controller'] == "Sponsors") {
                        echo "active";
                    }
                        ?>"><?php echo $this->Html->link(__('Sponsors'), array('controller' => 'Sponsors', 'action' => 'index')) ?></li>
                            <li class="<?php
                            if ($this->params['controller'] == "Rules") {
                                echo "active";
                            }
                        ?>"><?php echo $this->Html->link(__('Rules'), array('controller' => 'Rules', 'action' => 'index')) ?></li>
                            <li class="<?php
                    if ($this->params['controller'] == "Writeups") {
                        echo "active";
                    }
                        ?>"><?php echo $this->Html->link(__('Writeups'), array('controller' => 'Writeups', 'action' => 'index')) ?></li>
                        </ul>
                        <p style="margin-top: 10px;" class="<?php echo __('pull-right') ?>"><?php echo __("You're logged in as") ?> <?php echo h($this->Session->read('Auth.User.username')); ?> <?php echo $this->Html->link(__('logout'), array('controller' => 'users', 'action' => 'logout')) ?></p>
                    <?php endif; ?>
                    <?php else: ?>
                        <ul class="nav">

                            <li class="<?php
                    if ($this->params['controller'] == "Teams") {
                        echo "active";
                    }
                        ?>"><?php echo $this->Html->link(__('Scoreboard'), array('controller' => 'Teams', 'action' => 'index')) ?></li>
                            <li class="<?php
                    if ($this->params['controller'] == "Sponsors") {
                        echo "active";
                    }
                        ?>"><?php echo $this->Html->link(__('Sponsors'), array('controller' => 'Sponsors', 'action' => 'index')) ?></li>
                            <li class="<?php
                            if ($this->params['controller'] == "Rules") {
                                echo "active";
                            }
                        ?>"><?php echo $this->Html->link(__('Rules'), array('controller' => 'Rules', 'action' => 'index')) ?></li>
                            <li class="<?php
                    if ($this->params['controller'] == "Writeups") {
                        echo "active";
                    }
                        ?>"><?php echo $this->Html->link(__('Writeups'), array('controller' => 'Writeups', 'action' => 'index')) ?></li>
                        </ul>
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a class="dropdown-toggle"
                                   data-toggle="dropdown"
                                   href="#">
                                    <i class="icon-user icon-white"></i> <?php echo __('User'); ?>
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                                    <li><?php echo $this->Html->link(__('Login'), array('controller' => 'users', 'action' => 'login')); ?></li>
                                    <li><?php echo $this->Html->link(__('Reset Password'), array('controller' => 'users', 'action' => 'forgotPassword')); ?></li>
                                </ul>
                            </li>
                        </ul>
                    <?php endif; ?>

                </div>
            </div>

            <div class="row-fluid">
                <div class="row-fluid">
                    <div class="span12">
                        <?php echo $this->Session->flash(); ?>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <?php echo $this->fetch('content'); ?>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">

                    </div>
                </div>
            </div>
            <div class="row-fluid">

                <div class="span10 offset1">
                    <hr />
                </div>
            </div>
            <div class="row-fluid">

                <div class="span10 offset1">
                    <spane>Copyright © 2012 <a href="http://www.synapse-labs.com" rel="tooltip" title="Synase-Labs for R&D">Synapse-Labs</a> ® All Rights Reserved.</span>
                </div>
            </div>
        </div>
    </body>
    <script>
        $('.input.error').parent().parent().addClass('error');
        $('.input.error').addClass('control-group error');
        var inputs = $('.input.error');
        var error;
        for(var i = 0; i < inputs.length; i++){
            error = $(inputs[i]).children('.error-message').html();
            $(inputs[i]).children('.error-message').remove();
            $(inputs[i]).append('<span class="help-inline">'+error+'</span>');
        }
    </script>
</html>
