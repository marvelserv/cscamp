<div class="teamsLevels view">
<h2><?php  echo __('Teams Level'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($teamsLevel['TeamsLevel']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Team'); ?></dt>
		<dd>
			<?php echo $this->Html->link($teamsLevel['Team']['name'], array('controller' => 'teams', 'action' => 'view', $teamsLevel['Team']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Level'); ?></dt>
		<dd>
			<?php echo $this->Html->link($teamsLevel['Level']['name'], array('controller' => 'levels', 'action' => 'view', $teamsLevel['Level']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Teams Level'), array('action' => 'edit', $teamsLevel['TeamsLevel']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Teams Level'), array('action' => 'delete', $teamsLevel['TeamsLevel']['id']), null, __('Are you sure you want to delete # %s?', $teamsLevel['TeamsLevel']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Teams Levels'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Teams Level'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Teams'), array('controller' => 'teams', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Team'), array('controller' => 'teams', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Levels'), array('controller' => 'levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Level'), array('controller' => 'levels', 'action' => 'add')); ?> </li>
	</ul>
</div>
