<div class="span10 offset1">
    <div class="btn-group <?php echo __('pull-right'); ?>">
		<?php echo $this->Html->link(__('New Teams Level'), array('action' => 'add'), array('class' => 'btn btn-inverse')); ?>
		<?php echo $this->Html->link(__('List Teams'), array('controller' => 'teams', 'action' => 'index'), array('class' => 'btn btn-inverse')); ?>
		<?php echo $this->Html->link(__('New Team'), array('controller' => 'teams', 'action' => 'add'), array('class' => 'btn btn-inverse')); ?>
		<?php echo $this->Html->link(__('List Levels'), array('controller' => 'levels', 'action' => 'index'), array('class' => 'btn btn-inverse')); ?>
		<?php echo $this->Html->link(__('New Level'), array('controller' => 'levels', 'action' => 'add'), array('class' => 'btn btn-inverse')); ?>
    </div>
</div>
<div class="span10 offset1">
	<legend><?php echo __('Teams Levels'); ?></legend>
    <table class="table table-bordered table-striped">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('team_id'); ?></th>
			<th><?php echo $this->Paginator->sort('level_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	foreach ($teamsLevels as $teamsLevel): ?>
	<tr>
		<td><?php echo h($teamsLevel['TeamsLevel']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($teamsLevel['Team']['name'], array('controller' => 'teams', 'action' => 'view', $teamsLevel['Team']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($teamsLevel['Level']['name'], array('controller' => 'levels', 'action' => 'view', $teamsLevel['Level']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $teamsLevel['TeamsLevel']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $teamsLevel['TeamsLevel']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $teamsLevel['TeamsLevel']['id']), null, __('Are you sure you want to delete # %s?', $teamsLevel['TeamsLevel']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
        <?php
        if ($this->Paginator->hasPage(2)) {
            echo '<div class = "well" style = "padding: 5px 20px 5px 20px !important">';
            echo $this->Paginator->prev('' . __('previous'), array(), null, array());
            echo $this->Paginator->numbers();
            echo $this->Paginator->next('' . __('next'), array(), null, array());
            echo '</div>';
        }
        ?>
	</div>
</div>
