<div class="row_fluid">
    <div class="span2 offset1">
        <ul class="nav nav-tabs nav-stacked">

            <li><?php echo $this->Html->link(__('List Categories'), array('action' => 'index')); ?></li>
            <li><?php echo $this->Html->link(__('List Levels'), array('controller' => 'levels', 'action' => 'index')); ?> </li>
            <li><?php echo $this->Html->link(__('New Level'), array('controller' => 'levels', 'action' => 'add')); ?> </li>
        </ul>
    </div>
</div>
<div class="span7">
    <?php echo $this->Form->create('Category', array('class' => 'form-horizontal')); ?>

    <legend><?php echo __('Add Category'); ?></legend>
    <div class="control-group">
        <div class="controls">
            <?php echo $this->Form->input('name'); ?>
        </div>
    </div>

    <div class="form-actions">

        <?php
        echo $this->Form->submit(
                __('Add'), array(
            'name' => 'submit',
            'class' => 'btn btn-large btn-block btn-primary',
            'div' => false,
            'style' => 'padding: 5px 53px'));
        ?>

    </div>
</div>

