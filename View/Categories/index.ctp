<div class="row-fluid">
    <div class="offset1 span10">
        <legend><?php echo __('Categories'); ?></legend>
        <table class="table table-bordered table-hover">


            <tr>
                <th><?php echo $this->Paginator->sort('id'); ?></th>
                <th><?php echo $this->Paginator->sort('name'); ?></th>
                <th><?php echo $this->Paginator->sort('created'); ?></th>
                <th><?php echo $this->Paginator->sort('modified'); ?></th>
                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
            <?php foreach ($categories as $category): ?>
                <tr>
                    <td><?php echo h($category['Category']['id']); ?>&nbsp;</td>
                    <td><?php echo h($category['Category']['name']); ?>&nbsp;</td>
                    <td><?php echo h($category['Category']['created']); ?>&nbsp;</td>
                    <td><?php echo h($category['Category']['modified']); ?>&nbsp;</td>
                    <td class="actions">
                        <?php echo $this->Html->link(__('View'), array('action' => 'view', $category['Category']['id'])); ?>
                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $category['Category']['id'])); ?>
                        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $category['Category']['id']), null, __('Are you sure you want to delete # %s?', $category['Category']['id'])); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
        <?php
        if ($this->Paginator->hasPage(2)) {
            echo '<div class = "well" style = "padding: 5px 20px 5px 20px !important">';
            echo $this->Paginator->prev('' . __('previous'), array(), null, array());
            echo $this->Paginator->numbers();
            echo $this->Paginator->next('' . __('next'), array(), null, array());
            echo '</div>';
        }
        ?>

    </div>
</div>
<div class="row-fluid">
    <div class="span10 offset1">

        <a href="<?php echo $this->Html->url(array("action" => "add")); ?>" class="btn btn-medium btn-primary pull-right"><?php echo __('Add new Category'); ?></a>

    </div>
</div>

