<?php

class DATABASE_CONFIG {

    function setDefaults() {
        if (gethostname() == 'sudo.com') {
            $this->default['login'] = 'cscamp_usr';
            $this->default['password'] = 'cscamp_pwd';
            $this->default['database'] = 'cscamp';
        }
    }

    public $default = array(
        'datasource' => 'Database/Mysql',
        'persistent' => false,
        'host' => 'localhost',
        'login' => 'root',
        'password' => 'aiaabowali',
        'database' => 'CTF',
    );

    public function __construct() {
        $this->setDefaults();
    }

}