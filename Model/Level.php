<?php
App::uses('AppModel', 'Model');
/**
 * Level Model
 *
 * @property Category $Category
 */
class Level extends AppModel {

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Category' => array(
			'className' => 'Category',
			'foreignKey' => 'category_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
    );
            public $hasAndBelongsToMany = array(
                'Team' => array(
                        'className' => 'Team',
                        'joinTable' => 'teams_levels',
                        'foreignKey' => 'level_id',
                        'associationForeignKey' => 'team_id',
                        'unique' => 'keepExisting',
                        'conditions' => '',
                        'fields' => '',
                        'order' => '',
                        'limit' => '',
                        'offset' => '',
                        'finderQuery' => '',
                        'deleteQuery' => '',
                        'insertQuery' => ''
                )
        );

}
