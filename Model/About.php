<?php
App::uses('AppModel', 'Model');
/**
 * About Model
 *
 */
class About extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'about';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'description' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
        ),
        
        'photo' => array(
        'rule' => array('isValidExtension', array('jpg', 'png')),
        'message' => 'File has an invalid extension. available extensions are jpg, png, gif and jpeg'
    )
    );

        public $actsAs = array(
        'Upload.Upload' => array(
            'photo' => array(
                'fields' => array(
                    'dir' => 'photo_dir'
                ),
                'thumbnailSizes' => array(
                'xvga' => '1024x768',
                'vga' => '640x480',
                'thumb' => '360x270'
                )
            )
        )
    );
}
